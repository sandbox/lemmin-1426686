jQuery(document).ready(function($)
{
	$thumb_tape = $('#ig_thumbs');
	$thumbs = $('#ig_thumbs .ig_thumb_vp');
	$image = $('#ig_preview_vp img');
	$text = $('#ig_text');
	$thumbs.click(function(){ig_activate($thumbs.index(this));});
	$thumbs.first().addClass('ig_active');
	//80 is the width+margin of the thumb view port
	var img_width = 80;
	var thumbs_vp_offset = parseInt($thumb_tape.css('left'));
	
	$thumb_tape.css('width', $thumbs.length*img_width + 'px');
	//alert($thumb_vps.length*parseInt($thumb_vps.first().css('width')));
	
	ig_activate = function (index)
	{
		$thumbs.unbind('click');
		$('#ig_right').unbind('click');
		$('#ig_left').unbind('click');
		
		//$thumbs = $('#ig_thumbs .ig_thumb_vp');
		$active = $('#ig_thumbs .ig_active');
		
		if (!parseInt(index) && index != 0)
			index = $thumbs.index($active)+1;
		else if (index < 0)
			index = $thumbs.length-1;
		if (index >= $thumbs.length)
			index = 0;
		$next = $thumbs.eq(index);

		$active.removeClass('ig_active');
		$next.addClass('ig_active');
		
		$image.fadeOut(150, function()
		{
			$this = $(this)
			$this.attr('src', $next.attr('data-url'));
			$this.css('left', $next.attr('data-left'));
			$this.css('top', $next.attr('data-top'));
			$this.css('width', $next.attr('data-width'));
			$this.css('height', $next.attr('data-height'));
			$this.fadeIn(150);
			$text.html($next.attr('data-text'));
			
			$thumbs.click(function(){ig_activate($thumbs.index(this));});
			$('#ig_right').click(ig_activate);
			$('#ig_left').click(function(){ig_activate($thumbs.index($('#ig_thumbs .ig_active'))-1);});
		});
		$thumb_tape.animate({left: thumbs_vp_offset-(index*img_width)}, 300);
	}
	
	ig_explode = function (url)
	{
		var imgobj = new Image();
		imgobj.src = url;
		var realheight = imgobj.height;
		var realwidth = imgobj.width;
		$body = $('body');
		$window = $(window);
		var vp_width = $window.width();
		var vp_height = $window.height();
		
		//If the image is bigger than the viewport
		if (realheight >= vp_height || realwidth >= vp_width)
		{
			var ratio = realwidth/realheight;
			if (ratio > 1)
			{
				realwidth = vp_width-50;
				realheight = realwidth/ratio;
			}
			else
			{
				realheight = vp_height-50;
				realwidth = realheight*ratio;
			}
		}
		//$body.css('height', '800px');
		offset = $(document).scrollTop();
		$body.css('overflow', 'hidden');

		$overlay = $(document.createElement('div'));
		$overlay.attr('id', 'ig_overlay');
		$overlay.css('top', offset+'px');
		$body.append($overlay);
		
		$img = $(document.createElement('img'));
		$img.attr('src', url);
		$img.attr('id', 'ig_image_exploded');
		$img.attr('width', realwidth);
		$img.attr('height', realheight);
		$img.css('margin-top', parseInt(vp_height)/2-(realheight/2));
		
		$img_con = $(document.createElement('div'));
		$img_con.attr('id', 'ig_image_exploded_con');
		$img_con.css('top', offset+'px');
		$img_con.css('width', vp_width);
		$img_con.css('height', vp_height);
		
		//$img_con_rel = $(document.createElement('div'));
		//$img_con_rel.attr('id', 'ig_image_exploded_con_rel');
		//$img_con_rel.css('width', $img_con.width());
		//$img_con_rel.css('height', $img_con.height());
		
		//$img_con_rel.append($img);
		$img_con.append($img);
		$body.append($img_con);
		
		$(document).mouseup(ig_implode);
	}
	
	ig_implode = function ()
	{
		$(document).unbind('click');
		$('#ig_overlay').remove();
		$('#ig_image_exploded_con').remove();
		$('body').css('overflow', 'scroll');
	}
	
	$('#ig_right').click(ig_activate);
	$('#ig_left').click(function(){ig_activate($thumbs.index($('#ig_thumbs .ig_active'))-1);});
	$image.click(function ()
	{
		ig_explode(this.src);
	});
	//Prevent selecting the images.
	$('#image_gallery').mousedown(function()
	{
		// cancel out any text selections
		document.body.focus();
		// prevent text selection in IE
		document.onselectstart = function (){ return false; };
		// prevent IE from trying to drag an image
		this.ondragstart = function(){ return false; };
		// prevent text selection (except IE)
		return false; 
	});
	ig_activate(0);
});